package task

import (
	"fmt"
	"strings"

	"gitlab.com/mai-public/gophercises/taskManager/internal/dbtasks"
	"gopkg.in/alecthomas/kingpin.v2"
)

// This is for subcommands to the task command

// ListCmd handles user input from the 'list' subcommand
func ListCmd(fp string) {
	err := dbtasks.Init(fp)
	kingpin.FatalIfError(err, "Error when initializing/reinitializing database.\n%v\n", err)

	tasks, err := dbtasks.ListAll(fp)
	kingpin.FatalIfError(err, "Error when getting list of tasks.\n%v\n", err)

	if len(tasks) == 0 {
		fmt.Println("No tasks!")
	} else {
		for i, t := range tasks {
			fmt.Printf(" %d. %s\n", i+1, t.Value)
		}
	}
}

// AddCmd handles user input from the 'add' subcommand
func AddCmd(fp string, task []string) {
	err := dbtasks.Init(fp)
	kingpin.FatalIfError(err, "Error when initializing/reinitializing database.\n%v\n", err)

	taskString := strings.Join(task, " ")
	err = dbtasks.AddTask(fp, taskString)
	kingpin.FatalIfError(err, "Error adding task\n%v\n", err)
}

// DoCmd handles user input from the 'do' subcommand
func DoCmd(fp string, ids []int64) {

	err := dbtasks.Init(fp)
	kingpin.FatalIfError(err, "Error when initializing/reinitializing database.\n%v\n", err)

	tasks, err := dbtasks.ListAll(fp)
	kingpin.FatalIfError(err, "Error getting lists of tasks.\n%v\n", err)
	for _, id := range ids {
		id := int(id)
		if id <= 0 || id > len(tasks) {
			kingpin.Errorf("id out of range. Must fulfill 1 <= NUMBER <= %d. Got %d.\n", len(tasks), id)
			continue
		}
		err = dbtasks.RemoveTask(fp, tasks[id-1].Key)
		if err != nil {
			kingpin.Errorf("Error when removing task\n%v\n", err)
		}

	}
}

// TestCmd does stuff
func TestCmd(v []string) {
	fmt.Println(v[0])
}
