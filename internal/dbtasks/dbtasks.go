package dbtasks

import (
	"encoding/binary"

	"github.com/boltdb/bolt"
)

var (
	db         *bolt.DB
	taskBucket = []byte("task")
)

// Task represents one entry in the task manager
type Task struct {
	Key   int
	Value string
}

// Init initializes the bolt task database file and bucket
func Init(fp string) error {
	var err error
	db, err = bolt.Open(fp, 0600, nil)
	defer db.Close()
	if err != nil {
		return err
	}
	err = db.Update(func(tx *bolt.Tx) error {
		_, err = tx.CreateBucketIfNotExists(taskBucket)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

//ListAll gives back all tasks
func ListAll(fp string) ([]Task, error) {
	var err error
	db, err = bolt.Open(fp, 0600, nil)
	defer db.Close()
	if err != nil {
		return nil, err
	}

	var tasks []Task
	err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(taskBucket)
		if err != nil {
			return err
		}
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			tasks = append(tasks, Task{
				Key:   btoi(k),
				Value: string(v),
			})

		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return tasks, nil
}

// AddTask adds a task to the bolt database
func AddTask(fp string, task string) error {
	var err error
	db, err = bolt.Open(fp, 0600, nil)
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(taskBucket)
		id64, _ := b.NextSequence()
		return b.Put(itob(int(id64)), []byte(task))
	})

}

// RemoveTask removes a task associated with an ID
func RemoveTask(fp string, key int) error {
	var err error
	db, err = bolt.Open(fp, 0600, nil)
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(taskBucket)
		return b.Delete(itob(key))
	})
}

// helpers

//int -> []byte
func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

//[]byte -> int
func btoi(b []byte) int {
	return int(binary.BigEndian.Uint64(b))
}
