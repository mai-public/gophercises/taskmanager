package main

import (
	"log"
	"os"
	"strings"

	"gitlab.com/mai-public/gophercises/taskManager/internal/task"

	"gopkg.in/alecthomas/kingpin.v2"
)

// Usage:
//   task [command]

// Available Commands:
//   add         Add a new task to your TODO list
//   do          Mark a task on your TODO list as complete
//   list        List all of your incomplete tasks

// Use "task [command] --help" for more information about a command.

var (
	listCommand = kingpin.Command("list", "Shows incomplete tasks")

	addCommand = kingpin.Command("add", "Add a new task to your TODO list")

	taskName    = addCommand.Arg("task_name", "Name/Description that defins the task. Use quotes to keep whitespace").Required().Strings()
	doCommand   = kingpin.Command("do", "Mark a task on your TODO list as complete")
	taskIDs     = doCommand.Arg("taskID", "Number (or space seperated numbers) that defines the task (see all numbers with the 'do' subcommand").Required().Int64List()
	testCommand = kingpin.Command("test", "test")
	testVar     = testCommand.Arg("testvar", "testvar").Strings()
)

func main() {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	dbPath := strings.Join([]string{home, "go_junk", "task.db"}, string(os.PathSeparator))
	_ = dbPath
	kingpin.Version("0.0.1")
	switch kingpin.Parse() {
	// add command
	case addCommand.FullCommand():
		task.AddCmd(dbPath, *taskName)
	// do command
	case doCommand.FullCommand():
		task.DoCmd(dbPath, *taskIDs)
	//list command
	case listCommand.FullCommand():
		task.ListCmd(dbPath)

	case testCommand.FullCommand():
		task.TestCmd(*testVar)
	}
}
